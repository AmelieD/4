<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">POPschool</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#">LIEN</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Trier par<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Par étudiant</a></li>
            <li><a href="#">Veille de la semaine</a></li>
            <li><a href="#">Veille du mois</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
				<li><a href="membre.php">Espace membre</a></li>
         <li><a href="day.php">Ajouter une veille</a></li>
	      <li><a href="logout.php">Se déconnecter</a></li>
      </ul>
    </div>
  </div>
</nav>
